"use strict"
// Типи даних існують "string", "number", "boolean", "undefined", "BigInt", "symbol"
// Різниця між "==" та "===" є те що "==" перевіряє на рівність, а "===" перевіряє на едентичність, тобто a == false це буде вірно, якщо а рівне 0 чи false, чи ""
//в той час якщо а === false то воно може дорівнювати тільки false;
// Оператори - це знаки, що виконують якісь функції між операндами, це може бути будь що, наприклад присвоєння, щось з арифметики, порівняння, тернарний оператор і тд.
let age;
let name;
do {
  age = +prompt ("How old are you?", 0);
  name = prompt("What is your name?", "Max");
} while (Number.isNaN(age) || name === "") {
  if (age < 18) {
      console.log("You are not allowed to visit this website")    
  } else if (age >= 18 && age <= 22) {
    let question = confirm ("Are you sure you want to continue?");
    if (question) {
      console.log("Welcome, " + name);
    } else {
      console.log ("You are not allowed to visit this website");
    }
  } else {
    console.log("Welcome, " + name);
  }
}